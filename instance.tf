resource "google_compute_instance" "bt0001" {
	name = "bt0001"
	machine_type = "f1-micro"
	zone = "${var.region_zone}"

	network_interface {
		#network = "${google_compute_network.platform.name}"
		subnetwork = "${google_compute_subnetwork.dev.name}"
		access_config {
			# 
		}
	}

	boot_disk {
		initialize_params {
			image = "centos-cloud/centos-7"
		}
	}
	
	metadata {
		sshKeys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
	}

	metadata_startup_script = <<SCRIPT
sudo du -
echo "[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/7/x86_64/
gpgcheck=0
enabled=1" > /etc/yum.repos.d/nginx.repo 
yum install nginx -y
service nginx start
SCRIPT

}
